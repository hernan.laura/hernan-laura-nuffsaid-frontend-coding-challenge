import React from 'react';
import Header from './header/Header';
import MessagePage from './messages/MessagePage';

const App: React.FC = () => {
  return (
    <>
      <Header title="nuffsaid.com Coding Challenge" />
      <MessagePage />
    </>
  );
}

export default App;

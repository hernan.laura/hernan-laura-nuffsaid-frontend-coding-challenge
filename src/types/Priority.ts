import { COLOR_ERROR, COLOR_INFO, COLOR_WARN } from "../styles/colors";

export enum Priority {
  Error,
  Warn,
  Info,
}

export const priorityColorsMap = new Map<Priority, string>();
priorityColorsMap.set(Priority.Error, COLOR_ERROR);
priorityColorsMap.set(Priority.Warn, COLOR_WARN);
priorityColorsMap.set(Priority.Info, COLOR_INFO);

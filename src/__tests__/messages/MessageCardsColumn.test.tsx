import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'jest-styled-components';
import React from 'react';
import MessageCardsColumn from '../../messages/MessageCardsColumn';
import { renderWithMessages } from '../../test-utils/renderWithMessages';
import { Priority } from '../../types/Priority';

describe('MessageCardsColumn tests', () => {
    async function testCardsLength(message: string, expected: number) {
        const infoCards = await screen.findAllByText(message, { exact: false });
        expect(infoCards.length).toBe(expected);
    }

    function testTitle(expected: string) {
        const title = screen.getByRole('heading', { level: 2 });
        expect(title.innerHTML).toBe(expected);
    }


    async function testCount(expected: number) {
        const count = screen.getByText('Count', { exact: false });
        expect(count.innerHTML).toBe(`Count ${expected}`);
    }
    test('it should render MessageCardsColumn with 2 MessageCard', async () => {
        const comp = renderWithMessages(
            <MessageCardsColumn priority={Priority.Error} />
        );

        expect(comp).toMatchSnapshot();

        await testCardsLength('test message error', 2);

        testTitle('Error Type 1');

        testCount(2);
    });


    test('it should render 3 MessageCard and remove one when clear button is clicked', async () => {
        renderWithMessages(
            <MessageCardsColumn priority={Priority.Info} />
        );

        await testCardsLength('test message info', 3);

        testTitle('Info Type 3');

        testCount(3);

        const secondCardButton = screen.getAllByRole('button')?.[1];
        userEvent.click(secondCardButton)

        await testCardsLength('test message info', 2);
        testCount(2);
    });
})
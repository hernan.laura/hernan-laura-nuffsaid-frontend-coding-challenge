import { render } from '@testing-library/react';
import React from 'react';
import { Message } from '../../Api';
import MessageCard from '../../messages/MessageCard';
import { Priority } from '../../types/Priority';

// Since this is a dumb component my test only consist in a snapshot, ut bot as a visual regresion test.
// My approach to dumb (or presentational) components is to create a snapshot when I have it ready and then if something changes the snapshot test will fail

describe('MessageCard tests', () => {
    test('it should render and Error Message', () => {
        const message: Message = {
            id: 'id',
            message: 'test message',
            priority: Priority.Error
        }
        const comp = render(<MessageCard message={message} onClearClick={() => { }} />);
        expect(comp).toBeTruthy();
        expect(comp).toMatchSnapshot();
    });

    test('it should render and Warn Message', () => {
        const message: Message = {
            id: 'id',
            message: 'test message',
            priority: Priority.Warn
        }
        const comp = render(<MessageCard message={message} onClearClick={() => { }} />);
        expect(comp).toBeTruthy();
        expect(comp).toMatchSnapshot();
    });

    test('it should render and Info Message', () => {
        const message: Message = {
            id: 'id',
            message: 'test message',
            priority: Priority.Info
        }
        const comp = render(<MessageCard message={message} onClearClick={() => { }} />);
        expect(comp).toBeTruthy();
        expect(comp).toMatchSnapshot();
    })
})
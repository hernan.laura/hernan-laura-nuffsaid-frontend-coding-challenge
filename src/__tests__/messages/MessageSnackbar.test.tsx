import { render, screen } from '@testing-library/react';
import React from 'react';
import MessageSnackbar from '../../messages/MessageSnackbar';

describe('MessageSnackbar tests', () => {

    beforeEach(() => jest.useFakeTimers());
    afterEach(() => jest.useRealTimers());

    test('it should render a Snackbar with "test message"', () => {
        const closeFn = jest.fn();
        const comp = render(<MessageSnackbar message="test message" onClose={closeFn} />);
        expect(comp).toBeTruthy();
        expect(comp).toMatchSnapshot();

        const messageSpan = screen.getByText('test message');
        expect(messageSpan.textContent).toBe("test message");
    });

    test('it should close after 2 seconds', () => {
        const closeFn = jest.fn();

        render(<MessageSnackbar message="test message" onClose={closeFn} />);

        expect(closeFn).not.toHaveBeenCalled();
        jest.advanceTimersByTime(1000);
        expect(closeFn).not.toHaveBeenCalled();
        jest.advanceTimersByTime(2000);
        expect(closeFn).toHaveBeenCalledTimes(1);
    });
});
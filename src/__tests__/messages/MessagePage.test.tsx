import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'jest-styled-components';
import React from 'react';
import MessagePage from '../../messages/MessagePage';
import { renderWithMessages } from '../../test-utils/renderWithMessages';

jest.mock("../../Api");

describe('MessageCardsColumn tests', () => {
    test('it should render 6 MessageCard in 3 different tables', async () => {
        const comp = renderWithMessages(
            <MessagePage />
        );

        expect(comp).toMatchSnapshot();

        const cards = screen.getAllByText('test message', { exact: false });
        expect(cards.length).toBe(6);

        const columns = screen.getAllByText('Type', { exact: false });
        expect(columns.length).toBe(3);
    });

    test('it should clear all the messages when Clear button is pressed', async () => {
        function testCards(expected: number) {
            const cards = screen.queryAllByText('test message', { exact: false });
            expect(cards.length).toBe(expected);
        }

        renderWithMessages(
            <MessagePage />
        );

        testCards(6);
        const clearButton = screen.getByTestId('clear-all-button');
        userEvent.click(clearButton);

        testCards(0);
    });

    // There should be a test for Start/Stop the subscription
})
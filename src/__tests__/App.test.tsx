import React from 'react';
import App from '../App';
import { renderWithMessages } from '../test-utils/renderWithMessages';

jest.mock("../Api");

test('renders learn react link', () => {
  const comp = renderWithMessages(
    <App />
  );
  expect(comp).toBeTruthy();
  expect(comp).toMatchSnapshot();
});

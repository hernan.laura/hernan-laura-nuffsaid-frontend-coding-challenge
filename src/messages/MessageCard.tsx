import { Button, Card, CardActions, CardContent, Typography } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';
import { Message } from '../Api';
import { priorityColorsMap } from '../types/Priority';

interface MessageCardProps {
    message: Message;
    onClearClick: (id: string) => void;
    bgColor?: string;
}

const StyledCard = styled(Card)`
    && {
        background-color: ${props => props.color};
        margin: 10px;
    }`

const StyledCardActions = styled(CardActions)`
    && {
        background-color: ${props => props.color};
        button {
            margin-left: auto;
        }
    }`

const MessageCard: React.FC<MessageCardProps> = ({ message, onClearClick }) => {
    const bgColor = priorityColorsMap.get(message.priority);

    return (
        <StyledCard color={bgColor}>
            <CardContent>
                <Typography>
                    {message.priority} - {message.message}
                </Typography>
            </CardContent>
            <StyledCardActions color={bgColor}>
                <Button onClick={() => onClearClick(message.id)}>Clear</Button>
            </StyledCardActions>
        </StyledCard>
    );
};
export default MessageCard

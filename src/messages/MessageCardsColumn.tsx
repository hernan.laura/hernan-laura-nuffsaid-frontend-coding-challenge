import styled from "styled-components";
import { useMessageContext } from "../store/message-context";
import { Priority } from "../types/Priority";
import MessageCard from "./MessageCard";

interface MessageCardsColumnProps {
    priority: Priority;
}

const ColumnContainer = styled.div`
    h2 {
        margin-bottom: 0px;
        margin-left: 10px;
    }

    p {
        margin-top: 0px;
        margin-bottom: 0px;
        margin-left: 10px;
    }
`

const MessageCardsColumn: React.FC<MessageCardsColumnProps> = ({ priority }) => {
    const { deleteMessage, filterMessagesByPriority } = useMessageContext();

    const messages = filterMessagesByPriority(priority);

    return <ColumnContainer>
        <h2>{Priority[priority]} Type {priority + 1}</h2>
        <p>Count {messages.length}</p>

        {messages.map?.(msg =>
            <MessageCard
                key={msg?.id}
                message={msg}
                onClearClick={deleteMessage}
            />
        )}
    </ColumnContainer>
}

export default MessageCardsColumn
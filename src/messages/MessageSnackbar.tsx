import { IconButton, Snackbar } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React, { useEffect } from 'react';

interface MessageSnackbarProps {
    message: string;
    onClose: () => void;
}

const MessageSnackbar: React.FC<MessageSnackbarProps> = ({ message, onClose }) => {
    useEffect(() => {
        const timer = setTimeout(() => onClose(), 2000);

        return () => clearTimeout(timer)
    }, [message, onClose])

    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
            open={!!message}
            onClose={onClose}
            ContentProps={{
                'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{message}</span>}
            action={[
                <IconButton
                    aria-label="Close"
                    key="Close"
                    color="inherit"
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>,
            ]}
        />
    )
}

export default MessageSnackbar

import Button from '@material-ui/core/Button';
import React, { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';
import generateMessage, { Message } from '../Api';
import { useMessageContext } from '../store/message-context';
import { COLOR_INFO } from "../styles/colors";
import { Priority } from '../types/Priority';
import MessageCardsColumn from './MessageCardsColumn';
import MessageSnackbar from './MessageSnackbar';

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;

  button{
    margin: 8px 2px;
    color: #000;
    background-color: ${props => props.color};
  
    &:hover{
        background-color: ${props => props.color};
        opacity: 75%;
    }
  }
`
const Table = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
`

const MessagePage = () => {
    const [subscriptionActive, setSubscriptionActive] = useState(true)
    const [newErrorMessage, setNewErrorMessage] = useState<string | null>();
    const { addMessage, clearMessages } = useMessageContext();

    useEffect(() => {
        const cleanUp = generateMessage((message: Message) => {
            if (message.priority === Priority.Error) {
                setNewErrorMessage(message.message);
            }
            addMessage(message);
        });

        if (!subscriptionActive) {
            cleanUp();
        }

        return cleanUp;
    }, [addMessage, subscriptionActive]);

    const clearErrorMessage = useCallback(
        () => setNewErrorMessage(null),
        []
    )

    return (
        <>
            {newErrorMessage &&
                <MessageSnackbar message={newErrorMessage} onClose={clearErrorMessage} />
            }

            <ButtonContainer color={COLOR_INFO}>
                <Button variant="contained" color="primary" onClick={() => setSubscriptionActive(s => !s)}>
                    {subscriptionActive ? 'Stop' : 'Start'}
                </Button>
                <Button variant="contained" color="primary" data-testid="clear-all-button" onClick={clearMessages}>
                    Clear
                </Button>
            </ButtonContainer>

            <Table>
                <MessageCardsColumn priority={Priority.Error} />
                <MessageCardsColumn priority={Priority.Warn} />
                <MessageCardsColumn priority={Priority.Info} />
            </Table>
        </>
    )
}

export default MessagePage

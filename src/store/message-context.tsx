import React, { useCallback, useState } from 'react';
import { Message } from '../Api';
import { Priority } from '../types/Priority';

interface MessageStateContextProps {
    messages: Message[];
    addMessage: (message: Message) => void;
    deleteMessage: (id: string) => void;
    clearMessages: () => void;
    filterMessagesByPriority: (priority: Priority) => Message[];
}
const MessageStateContext = React.createContext<MessageStateContextProps | undefined>(undefined)

const MessageProvider: React.FC = ({ children }) => {
    const [messages, setMessages] = useState<Message[]>([]);

    const addMessage = useCallback(
        (message: Message) => setMessages(oldMessages => [message, ...oldMessages]),
        []
    )

    const deleteMessage = (id: string) => setMessages(oldMessages => oldMessages.filter(msg => msg.id !== id));

    const clearMessages = () => setMessages([]);

    const filterMessagesByPriority = (priority: Priority) => messages?.filter(msg => msg.priority === priority);

    const value = {
        messages,
        addMessage,
        deleteMessage,
        clearMessages,
        filterMessagesByPriority
    }
    return (
        <MessageStateContext.Provider value={value}>
            {children}
        </MessageStateContext.Provider>
    )
}

function useMessageContext() {
    const context = React.useContext(MessageStateContext)
    if (context === undefined) {
        throw new Error('useMessageContext must be used within a MessageProvider')
    }

    return context;
}

export { MessageProvider, useMessageContext };

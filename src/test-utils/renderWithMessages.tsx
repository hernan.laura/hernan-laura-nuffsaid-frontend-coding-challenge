import { render, RenderOptions } from '@testing-library/react';
import React, { ReactElement, useEffect } from 'react';
import { Message } from '../Api';
import { MessageProvider, useMessageContext } from '../store/message-context';
import { Priority } from '../types/Priority';

const Wrapper: React.FC = ({ children }) => {
  const { addMessage } = useMessageContext();

  useEffect(() => {
    const messages: Message[] = [{
      id: '1',
      message: 'test message info',
      priority: Priority.Info
    },
    {
      id: '2',
      message: 'test message info',
      priority: Priority.Info
    },
    {
      id: '3',
      message: 'test message info',
      priority: Priority.Info
    },
    {
      id: '4',
      message: 'test message error',
      priority: Priority.Error
    },
    {
      id: '5',
      message: 'test message error',
      priority: Priority.Error
    },
    {
      id: '6',
      message: 'test message warn',
      priority: Priority.Warn
    }];
    messages.forEach(m => addMessage(m));

  }, [addMessage])

  return <>{children}</>
}

const WithMessagesWrapper: React.FC = ({ children }) =>
  <MessageProvider>
    <Wrapper>
      {children}
    </Wrapper>
  </MessageProvider>;


const renderWithMessages = (
  ui: ReactElement<any>,
  options?: Omit<RenderOptions, 'wrapper'>
) => render(ui, { wrapper: WithMessagesWrapper, ...options });

export { renderWithMessages };

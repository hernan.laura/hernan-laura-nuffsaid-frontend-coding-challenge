import styled from 'styled-components'

interface Props {
    title: string
}
const Title = styled.h1`
        font-size: 1.5em;
        line-height: 1.75em;
        text-align: left;
        border-bottom: 1px solid #000;
        margin: 0;
    `;

const Header: React.FC<Props> = ({ title }) => {

    return (
        <Title>{title}</Title>
    )
}

export default Header
